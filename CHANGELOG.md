# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.3"></a>
## [0.2.3](https://bitbucket.org/codilogy/hapi-datasource/compare/v0.2.2...v0.2.3) (2017-06-22)


### Bug Fixes

* remotes for model are being exposed several times ([ba0e8d8](https://bitbucket.org/codilogy/hapi-datasource/commits/ba0e8d8))



<a name="0.2.2"></a>
## [0.2.2](https://bitbucket.org/codilogy/hapi-datasource/compare/v0.2.1...v0.2.2) (2017-06-20)



<a name="0.2.1"></a>
## [0.2.1](https://bitbucket.org/codilogy/hapi-datasource/compare/v0.2.0...v0.2.1) (2017-06-18)



<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/codilogy/hapi-datasource/compare/v0.1.0...v0.2.0) (2017-06-18)


### Bug Fixes

* cannot find module after moving index.js to plugin dir ([0a6f355](https://bitbucket.org/codilogy/hapi-datasource/commits/0a6f355))
* move `once` option from package.json to plugin `register.attributes` ([afeab95](https://bitbucket.org/codilogy/hapi-datasource/commits/afeab95))
* number of allowed values in range operators ([8bd8d01](https://bitbucket.org/codilogy/hapi-datasource/commits/8bd8d01))


### Features

* adds deep validation of include queries ([316354a](https://bitbucket.org/codilogy/hapi-datasource/commits/316354a))
* emit event when model has been defined ([c99dca6](https://bitbucket.org/codilogy/hapi-datasource/commits/c99dca6))
* initial support for scopes ([6225c2b](https://bitbucket.org/codilogy/hapi-datasource/commits/6225c2b))
* options basic validation ([3fbb5b1](https://bitbucket.org/codilogy/hapi-datasource/commits/3fbb5b1))


### Performance Improvements

* generate schema for query only once ([7538359](https://bitbucket.org/codilogy/hapi-datasource/commits/7538359))



<a name="0.1.0"></a>
# 0.1.0 (2017-04-30)


### Features

* initial commit ([cbd0606](https://bitbucket.org/codilogy/hapi-datasource/commits/cbd0606))
