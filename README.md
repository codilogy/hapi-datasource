# hapi-datasource

[![JavaScript Standard Style](https://img.shields.io/badge/code_style-standard-green.svg)](https://standardjs.com/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-green.svg)](https://conventionalcommits.org/)
[![bitHound Overall Score](https://www.bithound.io/bitbucket/codilogy/hapi-datasource/badges/score.svg)](https://www.bithound.io/bitbucket/codilogy/hapi-datasource)
[![Coverage Status](https://coveralls.io/repos/bitbucket/codilogy/hapi-datasource/badge.svg)](https://coveralls.io/bitbucket/codilogy/hapi-datasource)

> Plugin for Hapi for registering and exposing Sequelize models as RESTful endpoints. 

## Features

- mounting models using glob expressions, ex `models/**`
- exposing remote methods: POST, PUT, PATCH, GET, DELETE
- custom remote methods
- model associations
- maps fields and validations to [Joi](https://www.npmjs.com/package/joi) objects

## Installation

hapi-datasource is installed together with [Sequelize](http://sequelizejs.com). However, you are required to install 
next to hapi-datasource database module ([mysql](https://www.npmjs.com/package/mysql), [pg](https://www.npmjs.com/package/pg), etc.), 
[the same way as it is required by Sequelize](http://docs.sequelizejs.com/en/v3/docs/getting-started/).

```
npm install --save @codilogy/hapi-datasource mysql
```

## Usage

### Options

### API

#### getModel

```js
const User = server.getModel('user')
```

#### registerModels

```js
server.registerModels({
  models: 'models/**'
}).then(() => {
  console.log('models registered')
})
```

#### registerModel

```js
// models/user.js
module.exports = (db, DataTypes, server) => {
  db.define('user', {
    // model definition
  })
}
```

```js
const User = require('./models/user')

server.registerModel(User).then(() => {
  console.log('model registered')
})
```

## TODO

- Enable/Disable specific remote methods.
- Allow different auth strategies on remote endpoints.
- Option to use sync.force on startup (now is on by default).
- Pass more options to Sequelize settings.

## License

The MIT License (MIT)

Copyright &copy; 2017 Codilogy, [http://codilogy.com](http://codilogy.com) <info@codilogy.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the 
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
