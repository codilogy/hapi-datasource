const applyScopeToModel = require('./scope.helper')

module.exports = (server, model) => (request, reply) => {
  const query = Object.assign({}, request.query)
  const scopedModel = applyScopeToModel(model, query.scope)

  delete query.scope

  if (Array.isArray(request.payload)) {
    scopedModel.bulkCreate(request.payload).then(reply)
  } else {
    scopedModel.create(request.payload).then(reply)
  }
}
