const applyScopeToModel = require('./scope.helper')

module.exports = (server, model) => (request, reply) => {
  const query = Object.assign({}, request.query)
  const scopedModel = applyScopeToModel(model, query.scope)

  delete query.scope

  if (request.params.id) {
    scopedModel.destroy({
      where: {
        [scopedModel.primaryKeyAttribute]: request.params.id
      }
    }).then(count => reply({ count }))
  } else {
    scopedModel.destroy({
      truncate: true
    }).then(count => reply({ count }))
  }
}
