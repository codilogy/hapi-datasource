module.exports = {
  create: require('./create'),
  destroy: require('./destroy'),
  read: require('./read'),
  update: require('./update'),
  upsert: require('./upsert')
}
