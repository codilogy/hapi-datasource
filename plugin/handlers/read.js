const applyScopeToModel = require('./scope.helper')
const Boom = require('boom')

// https://github.com/hapijs/hapi/blob/master/API.md#error-transformation
const emptyNotFound = (payload = null) => {
  const error = Boom.notFound()
  error.output.payload = payload

  return error
}

const replaceIncludes = (server, includeConfig, parentModel) => {
  return includeConfig.map((config) => {
    const patch = {}
    const associations = parentModel.associations

    patch.model = server.getModel(config.model)

    // Handle SequelizeEagerLoadingError
    // Use the 'as' keyword to specify the alias within include statement.
    for (let i in associations) {
      let {target, options} = associations[i]
      if (target === patch.model) {
        patch.as = options.as
      }
    }

    if (config.include) {
      patch.include = replaceIncludes(server, config.include, patch.model)
    }

    return Object.assign(config, patch)
  })
}

module.exports = (server, model) => (request, reply) => {
  const query = Object.assign({}, request.query)
  const scopedModel = applyScopeToModel(model, query.scope)
  const id = request.params.id

  delete query.scope

  if (query.include) {
    query.include = replaceIncludes(server, query.include, scopedModel)
  }

  if (id) {
    scopedModel.findById(id, query).then(instance => {
      if (instance === null) {
        reply(emptyNotFound())
      } else {
        reply(instance.toJSON())
      }
    })
  } else {
    scopedModel.findAll(query).then(instances => {
      if (instances === null || instances.length < 1) {
        reply(emptyNotFound([]))
      } else {
        reply(instances.map(i => i.toJSON()))
      }
    })
  }
}
