/**
 * Converts scope object from query to an array, which
 * can be easily used with Sequelize `.scope()` method.
 * @param scopeObj
 * @return {Array}
 */
function scopeAsArray (scopeObj) {
  if (typeof scopeObj === 'object' && scopeObj !== null) {
    return Object.keys(scopeObj).map(name => {
      const method = [].concat(scopeObj[name])
      method.unshift(name)
      return { method }
    })
  }

  return [scopeObj]
}

/**
 * Flattens multiple scopes passed as an array to the query.
 * @param {Array} scopes
 * @return {Array}
 */
function flattenScopes (scopes) {
  return scopes.reduce((flatArr, scopeObj) => flatArr.concat(scopeAsArray(scopeObj)), [])
}

/**
 *
 * @param model {Model} Sequelize Model instance
 * @param scope {Array|*}
 * @return {Model} Sequelize Model instance
 */
module.exports = function applyScopeToModel (model, scope) {
  if (typeof scope === 'undefined' || scope === null) {
    return model
  }

  if (Array.isArray(scope)) {
    return model.scope(flattenScopes(scope))
  } else if (scope !== false) {
    return model.scope(scopeAsArray(scope))
  } else if (scope === false) {
    return model.unscoped()
  }
}
