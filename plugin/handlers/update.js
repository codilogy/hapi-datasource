const {Model} = require('sequelize')
const applyScopeToModel = require('./scope.helper')
const PRIMARY_KEY_PROPERTY = 'primaryKeyField'

const toJson = instance => {
  if (instance instanceof Model) {
    return instance.toJSON()
  } else {
    return null
  }
}

module.exports = (server, model) => (request, reply) => {
  const query = Object.assign({}, request.query)
  const scopedModel = applyScopeToModel(model, query.scope)
  const id = request.params.id
  const newId = request.payload.id || request.params.id
  const where = {[scopedModel[PRIMARY_KEY_PROPERTY]]: id}

  delete query.scope

  scopedModel.update(request.payload, {where})
    .then(() => {
      model.findById(newId).then(instance => {
        const location = `${request.connection.info.uri}/${model.options.name.plural}/${newId}`

        if (id !== newId) {
          return reply.redirect(location)
        } else {
          return reply(toJson(instance))
        }
      })
    })
}
