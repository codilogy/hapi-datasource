const url = require('url')
const applyScopeToModel = require('./scope.helper')
const {mts} = require('../utils')
const {Model} = require('sequelize')
const Boom = require('boom')
const PRIMARY_KEY_PROPERTY = 'primaryKeyField'

const toJson = instance => {
  if (instance instanceof Model) {
    return instance.toJSON()
  } else {
    return null
  }
}

const queryFactory = (request, {scopedModel, id, newId}) => {
  const where = {[scopedModel[PRIMARY_KEY_PROPERTY]]: id}
  let payload = request.payload

  // If both {id}s are equal then we should create or replace resource
  // with this {id}. However if they differ we have two choices:
  // 1. Reject request as invalid.
  // 2. Update resource with {id} to {newId} and return 301 with Location
  //    of new resource. However if there is no resource with {id} then no
  //    update is performed. In this case 409 Conflict should be returned
  //    so user can decide what to do.
  // Here we choose to go with scenario 2 as both 301 and 409 responses
  // are allowed with PUT by RFC.
  if (id === newId) {
    // Making upsert without {id} in the payload would result with
    // new resource being created, under {id} different then given
    // in parameters, what is not expected behavior of PUT request.
    if (!payload[PRIMARY_KEY_PROPERTY]) {
      payload = Object.assign({}, payload, where)
    }
    return scopedModel.upsert(payload)
  } else {
    // Making update from PUT request, we should make it "silent" to avoid
    // overriding "updatedAt" by Sequelize.
    // TODO: [PUT] Should server take control over auto-generated fields?
    // If yes, then those fields should be probably disallowed in payload.
    return scopedModel.update(payload, {where, silent: true}).then(affected => {
      const [count] = affected
      if (count === 0) {
        return Boom.conflict(mts(`
          Cannot replace "${scopedModel.name}/${id}" because target does not exists.
          Please update either {id} in URL to replace existing resource or 
          {${payload[PRIMARY_KEY_PROPERTY]}} in payload to update resource to new {id}.
        `))
      }
      return affected
    })
  }
}

// MySQL returns 1 for inserted, 2 for updated http://dev.mysql.com/doc/refman/5.0/en/insert-on-duplicate.html. Postgres has been modded to do the same
// @see node_modules/sequelize/lib/query-interface.js@576
const CREATED = 1 // 201 Created
const UPDATED = 2 // 200 OK

module.exports = (server, model) => (request, reply) => {
  const query = Object.assign({}, request.query)
  const scopedModel = applyScopeToModel(model, query.scope)
  const isSQLite = model.sequelize.options.dialect === 'sqlite'
  const id = request.params.id
  const newId = request.payload.id || request.params.id

  delete query.scope

  // Note that SQLite returns undefined for created, no matter if the row was created
  // or updated. This is because SQLite always runs INSERT OR IGNORE + UPDATE,
  // in a single query, so there is no way to know whether the row was inserted or not.
  let checkIfExists
  if (isSQLite) {
    checkIfExists = model.findById(id).then(instance => {
      return (instance === null) ? CREATED : UPDATED
    })
  } else {
    checkIfExists = Promise.resolve()
  }

  checkIfExists.then((exists) => {
    queryFactory(request, { scopedModel, id, newId })
      .then((status) => {
        if (status && status.isBoom) {
          return reply(status)
        }

        if (!status && exists) {
          status = exists
        } else {
          server.log(mts(`
            Response code for "${request.method.toUpperCase()} ${url.format(request.url)}" 
            might have been not set properly.
          `))
        }

        model.findById(newId).then(instance => {
          const location = `${request.connection.info.uri}/${model.options.name.plural}/${newId}`

          if (status === CREATED) {
            return reply(toJson(instance)).created(location)
          } else if (id !== newId) {
            return reply.redirect(location)
          } else {
            return reply(toJson(instance))
          }
        })
      })
  })
}
