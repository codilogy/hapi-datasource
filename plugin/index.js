const {Sequelize, DataTypes} = require('sequelize')
const globby = require('globby')
const debug = require('debug')('datasource')
const pkg = require('../package.json')
const optionsValidator = require('./schema-mapper/options')
const remoting = require('./remoting')
const {mts, isFunction} = require('./utils')

const MODEL_EVENT_NAME = 'model'
const MODEL_ASSOCIATION_SETTINGS_FIELD = 'references'
const MODEL_SYNC_OPTIONS = {
  force: true,
  alter: false
}

exports.register = (server, options, next) => {
  const {error, value} = optionsValidator(options)

  if (error) {
    return next(error)
  }

  // register custom event
  server.event(MODEL_EVENT_NAME)

  const config = value
  const settings = Object.assign({}, config.settings, {
    logging: (data) => {
      server.log([pkg.name, config.name], data)
    }
  })

  // @see http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor
  const db = new Sequelize(config.connection, settings)
  const models = db.models

  const getModel = (modelName) => models[modelName]

  const getModels = (...names) => {
    if (names === null) {
      names = Object.keys(models)
    }
    return names.reduce((map, name) => {
      map[name] = getModel(name)
      return map
    }, {})
  }

  function createModel (factory) {
    if (!isFunction(factory)) {
      throw new Error(mts(`
        Model module should return factory function with the following
        signature: "function (db: Sequelize, dt: DataTypes, srv: Hapi): Model"
      `))
    }

    const model = factory(db, DataTypes, this)

    server.emit(MODEL_EVENT_NAME, { model })

    return model
  }

  const importModel = (file) => {
    debug(`importing model from ${file}`)
    try {
      return createModel.call(server, require(file))
    } catch (error) {
      throw error
    }
  }

  const importModels = (patterns = ['models/**']) => {
    debug(`looking for models using pattern(s): ${patterns}`)
    return globby(patterns, {
      nodir: true,
      realpath: true
    }).then(files => files.map(importModel))
  }

  const hasAssociations = (model) => {
    return model && typeof model[MODEL_ASSOCIATION_SETTINGS_FIELD] !== 'undefined'
  }

  /**
   *
   * @param server {Sequelize}
   * @param models {Array<Model>}
   * @return {Promise.<Array<Model>>}
   */
  const associateModels = (server, models) => {
    if (!Array.isArray(models)) {
      throw new TypeError('models have to be an array of Models')
    }

    return Promise.all(models
      .filter(hasAssociations)
      .map(model => {
        Object.keys(model[MODEL_ASSOCIATION_SETTINGS_FIELD]).reduce((associations, name) => {
          const association = model[MODEL_ASSOCIATION_SETTINGS_FIELD][name]
          const options = Object.assign({as: name}, association.options || {})

          associations[model.name] = associations[model.name] || {}
          associations[model.name][name] = model[association.type](getModel(association.model), options)
          debug(`association ${model.name}.${association.type}(${association.model}) created as "${name}"`)

          return associations
        }, {})

        return model
      }))
  }

  /**
   *
   * @param server {Sequelize}
   * @param models {Array<Model>}
   * @return {Promise.<Array<Model>>}
   */
  const exposeRemoteMethods = (server, models) => new Promise((resolve) => {
    if (!Array.isArray(models)) {
      throw new TypeError('models have to be an array of Models')
    }

    models
      .map(model => {
        debug(`exposing default remote methods for "${model.name}" model`)
        remoting(server, model, config.remoting)

        return model
      })
      .filter(model => isFunction(model.remoteMethods))
      .forEach(model => {
        debug(`exposing custom remote methods for "${model.name}" model`)
        model.remoteMethods(server, db, model)
      })

    resolve(models)
  })

  /**
   *
   * @param {object} context Hapi.js server instance
   * @param {string[]} patterns Glob patterns used to search for models
   */
  const registerModels = (context, patterns) => db.authenticate()
    .then(() => importModels(patterns))
    .then((models) => {
      // Associate & expose imported models only
      return associateModels(context, models)
        .then(() => exposeRemoteMethods(context, models))
    })
    .then(() => db.sync(MODEL_SYNC_OPTIONS))

  server.expose(config.name, db)
  debug(`exposed as "${config.name}"`)

  server.decorate('server', 'getModel', getModel)

  server.decorate('server', 'getModels', getModels)

  server.decorate('server', 'registerModels', function (options) {
    return registerModels(this, options.models)
  })

  server.decorate('server', 'associateModels', function (models) {
    return associateModels(this, models).then(models => {
      // After associations are created, resync model with database
      return Promise.all(models.map((model) => {
        return model.sync(MODEL_SYNC_OPTIONS)
      }))
    })
  })

  server.decorate('server', 'exposeRemoteMethods', function (models) {
    return exposeRemoteMethods(this, models)
  })

  server.decorate('server', 'registerModel', function (factory) {
    return db.authenticate()
      .then(() => {
        return createModel.call(this, factory)
      })
      .then(model => {
        if (!model) {
          throw Error(mts(`
            Model is probably incorrectly defined. Check if you are returning model instance from factory function.
            Example: "return db.define('MyModel', {...})".
          `))
        } else {
          debug(`model "${model.name}" registered by registerModel()`)
        }

        if (hasAssociations(model)) {
          server.log(mts(`
            Model "${model.name}" has associated models. However when adding model through "registerModel"
            function you have to run associations and remoting manually. After adding models just run in order
            "server.associateModels()" and then "server.exposeRemoteMethods()".
          `))
        } else if (isFunction(model.remoteMethods)) {
          return exposeRemoteMethods(this, [model])
        }

        return [model]
      }).then(models => {
        return Promise.all(models.map(model => model.sync(MODEL_SYNC_OPTIONS)))
      })
  })

  if (config.models) {
    registerModels(server, config.models)
      .tap(() => debug(`"${config.name}" is ready`))
      .then(() => next())
      .catch(next)
  } else {
    next()
  }
}

exports.register.attributes = {
  pkg,
  once: true
}
