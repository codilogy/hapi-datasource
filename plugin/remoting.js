const SchemaMapper = require('./schema-mapper')
const handlers = require('./handlers')
const Joi = require('./schema-mapper/joi')
const Hoek = require('hoek')
const kebabCase = require('lodash.kebabcase')

const validate = (method, model, ...args) => (value, options, next) => {
  const schema = SchemaMapper[method](model, ...args)
  const validation = Joi.validate(value, schema)

  next(validation.error, validation.value)
}

const METHODS_ACCEPTING_ID = ['GET', 'PUT', 'PATCH', 'DELETE']
const METHODS_WITH_OPTIONAL_ID = ['GET', 'PATCH', 'DELETE']

const generateKey = (model, method) => {
  let key = model.primaryKeyField
  if (METHODS_WITH_OPTIONAL_ID.includes(method)) {
    key += '?'
  }

  return key
}

const generatePath = (model, method) => {
  if (typeof model.tableName === 'undefined') {
    throw new TypeError(`Cannot read "tableName" or "tableName" is not set on model "${model.name}"`)
  }

  let url = kebabCase(model.tableName)
  let path = `/${url}`

  if (METHODS_ACCEPTING_ID.includes(method)) {
    path += `/{${generateKey(model, method)}}`
  }

  return path
}

module.exports = (server, model, options) => {
  const validateQuery = validate('query', model)
  /**
   * Options passed to SchemaMapper for payload validation
   * of different operations.
   */
  const validatePayloadOptions = {
    create: { dual: true },
    upsert: {},
    update: { presence: 'optional' }
  }

  const route = (method, handler) => {
    let key = generateKey(model, method)
    let path = generatePath(model, method)

    return {
      method,
      path,
      handler: handlers[handler](server, model),
      config: Hoek.applyToDefaults({
        id: `${model.name}.${handler}`,
        validate: {
          query: validateQuery,
          params: METHODS_ACCEPTING_ID.includes(method)
            ? validate('params', model, key)
            : false,
          payload: validatePayloadOptions[handler]
            ? validate('payload', model, validatePayloadOptions[handler])
            : false
        }
      }, options.config || {})
    }
  }

  const routes = [
    route('POST', 'create'),
    route('GET', 'read'),
    route('PUT', 'upsert'),
    route('PATCH', 'update'),
    route('DELETE', 'destroy')
  ]

  routes.map((route) => {
    server.route(route)
  })
}
