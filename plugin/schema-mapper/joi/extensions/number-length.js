module.exports = (joi) => ({
  base: joi.number(),
  name: 'number',
  language: {
    length: 'length must be less than or equal to {{n}} characters long'
  },
  rules: [
    {
      name: 'length',
      params: {
        length: joi.number().positive().integer().required()
      },
      validate (params, value, state, options) {
        if (String(Math.abs(value)).length > params.length) {
          return this.createError('number.length', { v: value, n: params.length }, state, options)
        }

        return value
      }
    }
  ]
})
