const Joi = require('joi')
const numberLength = require('./extensions/number-length')

module.exports = Joi.extend([
  numberLength
])
