const Joi = require('../')

/**
 * Shorthand for making Joi schema which can accept single value
 * of given data type or an array of values of given data type.
 *
 * @param type {object} Joi schema
 * @returns {object} Joi schema
 */
module.exports = function dualSchema (type, min = 1) {
  return Joi.alternatives().try(
    Joi.array().min(min).items(type),
    type
  )
}
