const Joi = require('../')

class OperatorSchema {
  constructor (type) {
    this._type = type || Joi.any()
  }

  get genericComparision () {
    return this._type
  }

  get numericComparision () {
    return Joi.number()
  }

  get numericRange () {
    return Joi.array().length(2).items(Joi.number())
  }

  get stringComparision () {
    return Joi.alternatives().try(
      Joi.string(),
      Joi.object({
        $any: Joi.array().min(2).items(Joi.any())
      }))
  }

  get strictTypeRange () {
    return Joi.array().min(1).items(this._type)
  }

  get looseTypeRange () {
    return Joi.array().min(1).items(Joi.any())
  }
}

module.exports = OperatorSchema
