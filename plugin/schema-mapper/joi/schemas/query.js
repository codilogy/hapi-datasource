const Joi = require('../')
const OperatorSchema = require('../schemas/operator')
const dualSchema = require('../schemas/dual')
const mapping = require('../../mapping')

/**
 * Generate Joi schema for operators available in Sequelize.
 * It is possible to pass attribute type which will be used
 * as a required type on operators which would default to any type.
 *
 * @see http://docs.sequelizejs.com/en/v3/docs/querying/#operators
 *
 * @param [attributeType] {object} Joi schema
 * @returns {object} Joi schema
 */
const operatorsSchema = (attributeType) => {
  const type = new OperatorSchema(attributeType)

  return Joi.object().keys({
    // GENERIC_COMPARISION_OPERATORS
    $eq: type.genericComparision,
    $ne: type.genericComparision,

    // NUMBER_COMPARISION_OPERATORS
    $lt: type.numericComparision,
    $lte: type.numericComparision,
    $gt: type.numericComparision,
    $gte: type.numericComparision,

    // NUMERIC_RANGE_OPERATORS
    $between: type.numericRange,
    $notBetween: type.numericRange,

    // STRICT_TYPE_RANGE_OPERATORS
    $in: type.strictTypeRange,
    $notIn: type.strictTypeRange,

    // STRING_COMPARISION_OPERATORS
    $like: type.stringComparision,
    $notLike: type.stringComparision,
    $iLike: type.stringComparision,
    $notILike: type.stringComparision,

    // LOOSE_TYPE_RANGE_OPERATORS
    $any: type.looseTypeRange
  })
}

/**
 * Provides Joi schema for where query. It is build from attributes
 * schema, operators schema plus conditional operators ($and & $or).
 * Conditional operators are most difficult as they can be objects
 * or arrays of objects, where object is build again form attributes
 * schema, operators schema and nested conditional operators.
 *
 * Nested $or/$and is limited in the way it shouldn't allow to use
 * properties in properties. For example this will not pass (will end
 * in validation error: "$or" at position 0 fails because ["id" is not
 * allowed]):
 *
 * @code where={"$and": [{"id": {"$or": [{"id": 1}, {"id": 2}]}}]}
 *
 * However following code should work:
 *
 * @code where={"$and": [{"id": {"$or": [{"$eq": 1}, {"$eq": 2}]}}]}
 *
 * @param attributesSchema {object} Joi schema
 * @param attributeType {object} Joi schema
 * @returns {object} Joi schema
 */
const whereSchema = (attributesSchema, attributeType) => {
  const operators = operatorsSchema(attributeType)
  const attributesWithOperators = attributesSchema.concat(operators)
  const attributesWithOperatorsAlt = dualSchema(attributesWithOperators)
  const joinSchema = dualSchema(Joi.object().keys({
    $or: attributesWithOperatorsAlt,
    $and: attributesWithOperatorsAlt
  }).concat(attributesWithOperators))

  return Joi.object().keys({
    $or: joinSchema,
    $and: joinSchema
  }).concat(attributesWithOperators)
}

/**
 * Generates attributes schema from Sequelize model attributes.
 *
 * @param attributes {object} Sequelize model attributes
 * @returns {object} Joi schema
 */
const attributesSchema = (attributes) => {
  return Object.keys(attributes).reduce((schema, name) => {
    const type = Joi[mapping(attributes[name].type.key)]()
    return schema.keys({
      [name]: Joi.alternatives()
        .try(type)
        .try(whereSchema(schema, type))
    })
  }, Joi.object())
}

/**
 * Generates schema to test if include contains data specific to
 * related models.
 *
 * @see http://docs.sequelizejs.com/class/lib/associations/base.js~Association.html
 *
 * @param associations {object|*} Sequelize associations
 * @return {object} Joi schema
 */
const includeSchema = (associations, nested = true) => {
  if (!associations) {
    return Joi.any().forbidden()
  }

  const associatedModels = Object
    .keys(associations)
    .map(name => associations[name].target)

  if (associatedModels.length > 0) {
    const associationSchema = associatedModels.map(model => {
      const schema = {
        model: model.name,
        where: whereSchema(attributesSchema(model.attributes)),
        attributes: Joi.array().items(Object.keys(model.attributes))
      }

      if (nested && model.associations) {
        // TODO Support more nesting levels of include
        schema.include = includeSchema(model.associations, false)
      }

      return Joi.object().keys(schema)
    })

    return Joi.array().items(associationSchema)
  } else {
    return Joi.any().forbidden()
  }
}

/**
 * Returns schema for remote methods queries.
 *
 * @see http://docs.sequelizejs.com/en/v3/docs/querying/
 *
 * @param model {object} Sequelize model
 * @returns {object} Joi schema
 */
const querySchema = (model) => {
  return Joi.object().keys({
    where: whereSchema(attributesSchema(model.attributes)),
    attributes: Joi.array().items(Object.keys(model.attributes)),
    include: includeSchema(model.associations),
    offset: Joi.number(),
    limit: Joi.number(),
    order: dualSchema(Joi.string(), 2),
    scope: Joi.alternatives().try(false, Joi.array().items(
      Joi.object(),
      Joi.string()
    ).single())
  })
}

module.exports = querySchema
