/**
 * Copyright Codilogy 2017. All Rights Reserved.
 * Node module: @codilogy/hapi-datasource
 * This file is licensed under the MIT License.
 * License text available at https://opensource.org/licenses/MIT
 */

const debug = require('debug')('datasource:wrapper')
const Joi = require('./index')
const {isFunction} = require('../../utils')

/**
 * Wraps Joi schema of specific data type (array, string, number, etc.)
 * and allows easy updates of already created schema.
 * @type {JoiWrapper}
 */
class JoiWrapper {
  constructor (type) {
    this.type = type
    this.schema = this.generateSchema()
  }

  /**
   * Create new Joi schema of specific data type.
   * @return {*}
   */
  generateSchema () {
    return Joi[this.type]()
  }

  /**
   * Apply additional property on Joi schema.
   *
   * @example
   * const email = new JoiWrapper('string')
   * email.set('email', {
   *   errorLevel: 2
   * })
   *
   * @param property {string} Valid Joi data type method name.
   * @param [options] Options supported by method.
   * @return {JoiWrapper}
   */
  set (property, ...options) {
    debug(this.type, property, ...options)
    const schema = this.generateSchema()

    if (isFunction(schema[property])) {
      const schemaModifier = schema[property](...options)

      this.schema = this.schema.concat(schemaModifier)
    } else {
      console.error(`"Joi.${this.type}" has no method "${property}"`)
    }

    return this
  }
}

module.exports = JoiWrapper
