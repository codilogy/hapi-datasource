/**
 * Maps Sequelize data type to Joi data type.
 * @param type {string}
 * @returns {string}
 */
module.exports = function map (type) {
  switch (type) {
    case 'INTEGER':
    case 'BIGINT':
    case 'FLOAT':
    case 'REAL':
    case 'DOUBLE':
    case 'DECIMAL':
      return 'number'
    case 'STRING':
    case 'TEXT':
    case 'UUID':
      return 'string'
    case 'DATE':
    case 'DATEONLY':
      return 'date'
    case 'BOOLEAN':
      return 'boolean'
    default:
      return 'any'
  }
}
