const Joi = require('joi')

/**
 * @see http://docs.sequelizejs.com/class/lib/model.js~Model.html#static-method-init
 */
const modelOptionsChema = Joi.object().keys({
  defaultScope: Joi.object(),
  scopes: Joi.object(),
  omitNull: Joi.boolean(),
  timestamps: Joi.boolean().default(true),
  paranoid: Joi.boolean().default(false),
  underscored: Joi.boolean().default(false),
  underscoredAll: Joi.boolean().default(false),
  freezeTableName: Joi.boolean().default(false),
  name: Joi.object().keys({
    singular: Joi.string(),
    plural: Joi.string()
  }),
  indexes: Joi.array().items([
    Joi.object().keys({
      name: Joi.string(),
      type: Joi.string(),
      method: Joi.string(),
      unique: Joi.boolean().default(false),
      concurrently: Joi.boolean().default(false),
      fields: Joi.array()
    })
  ]),
  createdAt: Joi.alternatives().try(Joi.string(), Joi.boolean()),
  updatedAt: Joi.alternatives().try(Joi.string(), Joi.boolean()),
  deletedAt: Joi.alternatives().try(Joi.string(), Joi.boolean()),
  tableName: Joi.string(),
  schema: Joi.string(), // .default('public'),
  engine: Joi.string(),
  charset: Joi.string(),
  comment: Joi.string(),
  collate: Joi.string(),
  initialAutoIncrement: Joi.string(),
  hooks: Joi.object(),
  validate: Joi.object()
})

const dataSourceSchema = Joi.object().keys({
  name: Joi.string().required(),
  connection: Joi.string(),
  models: Joi.array().items(Joi.string()),
  // @see http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor
  settings: Joi.object().keys({
    host: Joi.string().default('localhost'),
    port: Joi.number(),
    username: Joi.string().default(null),
    password: Joi.string().default(null),
    database: Joi.string().default(null),
    dialect: Joi.string().default('mysql'),
    dialectModulePath: Joi.string().default(null),
    dialectOptions: Joi.object(),
    storage: Joi.string().default(':memory:'),
    protocol: Joi.string().default('tcp'),
    define: modelOptionsChema,
    query: Joi.object().default({}),
    set: Joi.object().default({}),
    sync: Joi.object().default({}),
    timezone: Joi.string().default('+00:00'),
    logging: Joi.forbidden().description(`
      "logging" option is controlled by is controlled by hapi-datasource plugin
      and it defaults to server.log.
    `),
    benchmark: Joi.boolean().default(false),
    omitNull: Joi.boolean().default(false),
    native: Joi.boolean().default(false),
    replication: Joi.boolean().default(false),
    pool: Joi.object().keys({
      max: Joi.number().integer().default(5),
      min: Joi.number().integer().default(0),
      idle: Joi.number().integer(),
      acquire: Joi.number().integer(),
      validate: Joi.func()
    }),
    quoteIdentifiers: Joi.boolean().default(true),
    transactionType: Joi.string().default('DEFERRED'),
    isolationLevel: Joi.string(),
    retry: Joi.object().keys({
      match: Joi.array().items(Joi.string()),
      max: Joi.number().integer()
    }),
    typeValidation: Joi.boolean().default(false)
  }),
  remoting: Joi.object().keys({
    enabled: Joi.boolean(),
    config: Joi.object().description(`
      Settings passed to each route options of remote method.
      Those settings are the defaults and can be overridden by
      route specific settings.
      See: https://hapijs.com/api#route-options
    `)
  }).default({
    enabled: true,
    config: {}
  })
})

const optionsSchema = Joi.alternatives()
  .try(Joi.array().items(dataSourceSchema))
  .try(dataSourceSchema)

module.exports = (options) => {
  return Joi.validate(options, optionsSchema)
}
