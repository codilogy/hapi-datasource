
/**
 * Helper function to reformat Multiline Template Strings by removing
 * all additional spaces or line brakes. Helpful if you want to arrange input text
 * in many lines and keep code indents but display text as a single line message.
 * @param text {string}
 * @return {string}
 */
exports.mts = (text) => text.replace(/\t|\s{2,}|\r?\n/g, ' ').trim()

exports.isFunction = (fn) => fn && typeof fn === 'function'
