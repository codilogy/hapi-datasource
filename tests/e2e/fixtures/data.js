module.exports = [
  {
    id: 1,
    name: 'Blue',
    texts: [{
      id: 1,
      key: 'desc',
      value: 'Lorem ipsum dolor sit amet',
      lang: 'en',
      tags: [{
        name: 'lorem'
      }]
    }, {
      id: 2,
      key: 'desc',
      value: 'Drogi Marszałku, Wysoka Izbo.',
      lang: 'pl'
    }]
  },
  {
    id: 2,
    name: 'Red'
  },
  {
    id: 3,
    name: 'Dark red'
  },
  {
    id: 4,
    name: 'Green'
  },
  {
    id: 5,
    name: 'Light green'
  },
  {
    id: 6,
    name: 'Magenta',
    active: false
  }
]
