module.exports = (db, {STRING, BOOLEAN}) => {
  const Item = db.define('item', {
    name: STRING,
    active: { type: BOOLEAN, defaultValue: true }
  }, {
    defaultScope: {
      where: {
        active: true
      }
    },
    scopes: {
      max: {
        attributes: [ [ db.fn('max', db.col('id')), 'maxId' ] ]
      },
      search: (search) => {
        return {
          where: {
            name: {
              $like: `%${search}`
            }
          }
        }
      }
    }
  })

  Item.references = {
    texts: {
      type: 'hasMany',
      model: 'text'
    }
  }

  return Item
}
