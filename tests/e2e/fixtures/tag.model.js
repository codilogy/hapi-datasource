module.exports = (db, {STRING}) => {
  const Tag = db.define('tag', {
    name: STRING
  })

  return Tag
}
