module.exports = (db, {STRING}) => {
  const Text = db.define('text', {
    key: STRING,
    value: STRING,
    lang: STRING(2)
  })

  Text.references = {
    items: {
      type: 'belongsTo',
      model: 'item'
    },
    tags: {
      type: 'hasMany',
      model: 'tag'
    }
  }

  return Text
}
