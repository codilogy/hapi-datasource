/* eslint-env node, mocha */

const Hapi = require('hapi')
const chai = require('chai')
const TestAgent = require('../helpers/test-agent')
const expect = chai.expect
const HOST = 'localhost'
const PORT = 6248
const URL = `http://${HOST}:${PORT}`
const PLUGIN = {
  register: require('../../plugin/index'),
  options: {
    name: 'sqlite',
    connection: 'sqlite://db.sqlite/api',
    settings: {
      storage: ':memory:'
    }
  }
}

const {item, text, tag} = require('./fixtures/models')
const data = require('./fixtures/data')

const agent = new TestAgent({
  ok: true,
  prefix: URL
})

describe('Remote', function () {
  let server

  before(() => {
    server = new Hapi.Server()
    server.connection({ port: PORT, host: HOST })
    server.on('log', (event, tags) => {
      // console.log(event.timestamp, tags)
    })
  })

  before(() => server.register(PLUGIN))
  before(() => server.registerModel(item))
  before(() => server.registerModel(text))
  before(() => server.registerModel(tag))
  before(() => {
    const {item, text} = server.getModels('item', 'text')

    return server.associateModels([item, text])
      .then(() => server.exposeRemoteMethods([item, text]))
  })
  before(() => server.start())

  beforeEach(() => {
    const {item, text, tag} = server.getModels('item', 'text', 'tag')
    const options = {
      include: [{
        model: text,
        as: 'texts',
        include: [{
          model: tag,
          as: 'tags'
        }]
      }]
    }

    return Promise.all([item, text, tag].map(m => m.sync({force: true})))
      .then(() => Promise.all(data.map(d => item.create(d, options))))
  })

  after(() => server.stop())

  describe('GET', () => {
    it('should return 404 for not found resource', () => {
      return agent.fetch('get', '/items/100').then(({status}) => {
        expect(status).to.be.eql(404)
      })
    })
    it('should return 404 for empty list of resources', () => {
      return agent.fetch('get', '/items?where={"id":{"$gt":100}}').then(({status}) => {
        expect(status).to.be.eql(404)
      })
    })
    it('should get one item', () => agent.fetch('get', '/items/3').then(({status, body}) => {
      expect(status).to.equal(200)
      expect(body).to.have.property('name', 'Dark red')
    }))

    it('should get all items', () => agent.fetch('get', '/items').then(({status, body}) => {
      expect(status).to.equal(200)
      expect(body).to.be.an('array').and.have.length(5)
    }))
  })

  describe('PUT', () => {
    it('should fail required model attributes are not specified', () => {
      return agent.fetch('put', '/items/1', {
        id: 1,
        active: true
      }).then(({status, body}) => {
        expect(status).to.be.at.least(400)
        expect(body).to.have.property('message')
          .that.contains('child "name" fails because ["name" is required]')
      })
    })
    it('should create not existing resource', () => {
      return agent.fetch('get', '/items/10').then(({status}) => {
        expect(status).to.be.eql(404)
      }).then(() => {
        return agent.fetch('put', '/items/10', {
          name: 'test'
        })
      }).then(({status}) => {
        expect(status).to.be.eql(201)
      }).then(() => {
        return agent.fetch('get', '/items/10')
      }).then(({status}) => {
        expect(status).to.be.eql(200)
      })
    })
    it('should update existing resource', () => {
      return agent.fetch('get', '/items/1').then(({body}) => {
        const previous = body
        return agent.fetch('put', '/items/1', Object.assign({}, previous, {
          name: 'More blue'
        })).then(({body}) => {
          expect(body).to.have.property('name', 'More blue')
          Object.keys(body).filter(key => key !== 'name').map(key => {
            expect(body).to.have.property(key, previous[key])
          })
        })
      })
    })
    it('should return status 200 & resource instance when existing resource is updated', () => {
      const payload = {
        id: 1,
        name: 'test',
        active: true,
        createdAt: '2000-12-31T23:00:00.000Z',
        updatedAt: '2000-12-31T23:00:00.000Z'
      }
      return agent.fetch('put', '/items/1', payload).then(({status, body}) => {
        expect(status).to.be.eql(200)
        expect(body).to.be.eql(payload)
      })
    })
    it('should return status 201 & resource instance when new resource is created', () => {
      const payload = {
        id: 11,
        name: 'test',
        active: true,
        createdAt: '2000-12-31T23:00:00.000Z',
        updatedAt: '2000-12-31T23:00:00.000Z'
      }
      return agent.fetch('put', '/items/11', payload).then(({status, body}) => {
        expect(status).to.be.eql(201)
        expect(body).to.be.eql(payload)
      })
    })
    it('should return status 200 (as a result of redirection) for resources with replaced {id}', () => {
      const payload = {
        id: 11,
        name: 'test',
        active: true,
        createdAt: '2000-12-31T23:00:00.000Z',
        updatedAt: '2000-12-31T23:00:00.000Z'
      }
      return agent.fetch('put', '/items/1', payload).then(({status, body, redirects}) => {
        expect(status).to.be.eql(200)
        expect(body).to.be.eql(payload)
        expect(redirects).to.have.length(1)
        return agent.fetch('get', '/items/1').then(({status}) => {
          expect(status).to.be.eql(404)
        })
      })
    })
    it('should return 409 Conflict when trying to put resource with {id(n)} on non existing resource {id(m)}', () => {
      const payload = {
        id: 110,
        name: 'test',
        active: true,
        createdAt: '2000-12-31T23:00:00.000Z',
        updatedAt: '2000-12-31T23:00:00.000Z'
      }
      return agent.fetch('put', '/items/10', payload).then(({status, body}) => {
        expect(status).to.be.eql(409)
        expect(body).to.have.property('message')
          .that.contains('Cannot replace "item/10" because target does not exists')
      })
    })
  })

  describe('POST', () => {
    it('should create one item', () => agent.fetch('post', '/items', {
      name: 'test'
    }).then(res => {
      expect(res.status).to.equal(200)
    }))

    it('should create bulk of items', () => agent.fetch('post', '/items', [
      {name: 'abc'},
      {name: 'xyz'}
    ]).then(res => {
      expect(res.status).to.equal(200)
      expect(res.body).to.be.an('array').and.have.length(2)
    }))
  })

  describe('PATCH', () => {
    it('should patch item with partial data', () => agent.fetch('patch', '/items/1', {
      active: false
    }).then(res => {
      expect(res.status).to.equal(200)
    }))

    it('should patch item', () => agent.fetch('patch', '/items/2', {
      name: '321'
    }).then(res => {
      expect(res.status).to.equal(200)
    }))
  })

  describe('DELETE', () => {
    it('should delete one item', () => agent.fetch('delete', '/items/2').then(res => {
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('count', 1)
    }))

    it('should delete all items', () => agent.fetch('delete', '/items').then(res => {
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('count', 5)
    }))
  })

  describe('queries validation', () => {
    const ERROR_RESPONSE_FIELDS = ['validation', 'message', 'error', 'statusCode']

    it('should fail when attribute in query has invalid type', () => agent
      .fetch('get', '/items?where={"id":"X"}').then(res => {
        expect(res.status).to.equal(400)
        expect(res.body).to.have.keys(ERROR_RESPONSE_FIELDS)
      })
    )
    it('should fail when unknown operator or attribute is used', () => agent
      .fetch('get', '/items?where={"X":"Y"}').then(res => {
        expect(res.status).to.equal(400)
        expect(res.body).to.have.keys(ERROR_RESPONSE_FIELDS)
      })
    )
    it('should allow nested queries', () => agent
      .fetch('get', '/items?where={"$or":[{"id":1},{"$and":[{"id":{"$gt":3}},{"id":{"$lt":9}}]}]}').then(res => {
        expect(res.status).to.equal(200)
        expect(res.body).to.have.length(3)
      })
    )
    it('should validate type in nested queries', () => agent
      .fetch('get', '/items?where={"$or":[{"id":"X"}]}').then(res => {
        expect(res.status).to.equal(400)
        expect(res.body).to.have.keys(ERROR_RESPONSE_FIELDS)
      })
    )
  })

  describe('where queries', () => {
    it('should allow to use attribute as raw value (short $eq)', () => agent
      .fetch('get', '/items?where={"id":1}').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(1)
          .and.have.nested.property('0.id', 1)
      }))

    it('should allow to use attribute as an object', () => agent
      .fetch('get', '/items?where={"id":{"$eq":1}}').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(1)
          .and.have.nested.property('0.id', 1)
      }))

    it('should allow to use attribute as an conditional object', () => agent
      .fetch('get', '/items?where={"$or":[{"id":1},{"id":2}]}').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(2)
        expect(res.body).to.have.nested.property('0.id', 1)
        expect(res.body).to.have.nested.property('1.id', 2)
      }))
  })

  describe('limit && offset queries', () => {
    it('should limit returned items to specified value', () => agent
      .fetch('get', '/items?limit=2').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(2)
        expect(res.body).to.have.nested.property('0.id', 1)
        expect(res.body).to.have.nested.property('1.id', 2)
      })
    )

    it('should limit returned items to specified by the offset', () => agent
      .fetch('get', '/items?offset=2').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(3)
        expect(res.body).to.have.nested.property('0.id', 3)
        expect(res.body).to.have.nested.property('2.id', 5)
      })
    )

    it('should limit returned items to specified value and offset', () => agent
      .fetch('get', '/items?limit=2&offset=2').then(res => {
        expect(res.body).to.be.an('array')
          .and.have.length(2)
        expect(res.body).to.have.nested.property('0.id', 3)
        expect(res.body).to.have.nested.property('1.id', 4)
      })
    )
  })

  describe('include query', () => {
    it('should load related model', () => agent
      .fetch('get', '/items/1?include=[{"model":"text"}]').then(res => {
        expect(res.body).to.have.property('texts')
          .and.have.length(2)
      }))

    it('should load related model with limited where and arguments', () => agent
      .fetch('get', '/items/1?include=[{"model":"text","where":{"lang":"pl"},"attributes":["key","value"]}]').then(res => {
        expect(res.body).to.have.property('texts')
          .and.have.length(1)
        expect(res.body).to.have.nested.property('texts[0].value', 'Drogi Marszałku, Wysoka Izbo.')
      }))

    it('should return error when trying to use unrelated model', () => agent
      .fetch('get', '/items?include=[{"model":"meta"}]').then(res => {
        expect(res.body)
          .to.have.nested.property('message')
          .that.contain('"model" must be one of [text]')
      }))

    it('should return error when trying to use unknown related model attributes', () => agent
      .fetch('get', '/items?include=[{"model":"text", "attributes": ["language"]}]').then(res => {
        expect(res.body)
          .to.have.nested.property('message')
          .that.contain('"attributes" at position 0 does not match any of the allowed types')
      }))

    it('should return error when trying to use unknown related model where properties', () => agent
      .fetch('get', '/items?include=[{"model":"text", "where": {"language": "pl"}}]').then(res => {
        expect(res.body)
          .to.have.nested.property('message')
          .that.contain('"where" fails because ["language" is not allowed]')
      }))

    it('should resolve deeply nested includes', () => agent
      .fetch('get', '/items/1?include=[{"model":"text","include":[{"model":"tag"}]}]').then(res => {
        expect(res.body).to.have.nested.property('texts[0].tags[0].name', 'lorem')
      })
    )

    it('should resolve deeply nested includes', () => agent
      .fetch('get', '/items/1?include=[{"model":"text","include":[{"model":"tags"}]}]').then(res => {
        expect(res.body)
          .to.have.nested.property('message')
          .that.contain('"include" at position 0 does not match any of the allowed types')
      })
    )
  })

  describe('scope query', () => {
    it('should accept name of scope to be applied', () => agent
      .fetch('get', '/items?scope=max').then(res => {
        expect(res.body).to.have.nested.property('0.maxId', 6)
      }))

    it('should accept scope to be applied with argument', () => agent
      .fetch('get', '/items?scope={"search": "red"}').then(res => {
        expect(res.body).to.have.length(2)
      }))

    it('should accept scope to be applied with arguments (as an array)', () => agent
      .fetch('get', '/items?scope={"search": ["red"]}').then(res => {
        expect(res.body).to.have.length(2)
      }))

    it('should accept multiple scopes', () => agent
      .fetch('get', '/items?scope=["max", {"search": "red"}]').then(res => {
        expect(res.body).to.have.nested.property('0.maxId', 3)
      }))

    it('should remove default scope when set to false', () => agent
      .fetch('get', '/items?scope=false').then(res => {
        expect(res.body).to.have.length(6)
      }))
  })
})
