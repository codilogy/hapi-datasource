const superagnet = require('superagent')
const debug = require('debug')('testagent:tests')

/**
 * Wrapper around superagnet that allows minimal setup which you don't
 * need to repeat with every query.
 */
class TestAgent {
  constructor (options) {
    this._prefix = options.prefix
    this._ok = options.ok

    this._agent = superagnet
  }

  fetch (method, url, data) {
    let uri = (this._prefix || '') + (url || '')
    let request = this._agent[method.toLowerCase()](uri)

    if (data) {
      request = request.send(data)
    }

    if (this._ok) {
      // https://visionmedia.github.io/superagent/#error-handling
      request = request.ok(res => {
        if (res.body && res.body.statusCode > 399) {
          debug(res.body)
        }
        return res.status < 500
      })
    }

    return request
  }
}

module.exports = TestAgent
