/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const expect = require('chai').expect
const Joi = require('joi')
const SchemaWrapper = require('../../plugin/schema-mapper/joi/wrapper')

describe('length extension to Joi number schema', () => {
  const VALID_LENGTH = 4
  const VALID_VALUES = [1234, 1.34, -1234, -1.34]
  const INVALID_VALUES = [12345, 123.5, -12345, -1.345]

  let wrapper, schema

  beforeEach(() => {
    wrapper = new SchemaWrapper('number')
    schema = wrapper.set('length', VALID_LENGTH).schema
  })

  it('should pass when value length is valid', () => {
    VALID_VALUES.map(value => {
      const result = Joi.validate(value, schema)

      expect(result).to.have.property('error', null)
      expect(result).to.have.property('value', value)
    })
  })

  it('should return error when value length is exceeded', () => {
    INVALID_VALUES.map(value => {
      const result = Joi.validate(value, schema)

      expect(result)
        .to.have.nested.property('error.message')
        .that.contains(`"value" length must be less than or equal to ${VALID_LENGTH} characters long`)
    })
  })

  it('should accept only positive integers as an option', () => {
    expect(() => {
      wrapper.set('length', 0)
    }).to.throw('"length" must be a positive number')
    expect(() => {
      wrapper.set('length', 0.5)
    }).to.throw('"length" must be an integer')
  })
})
