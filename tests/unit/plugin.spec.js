/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon')
const plugin = require('../../plugin/index')
const Hapi = require('hapi')
const HOST = 'localhost'
const PORT = 6248

chai.use(require('sinon-chai'))

describe('Plugin', () => {
  let server, next

  beforeEach('initialize plugin', () => {
    server = new Hapi.Server()
    server.connection({ port: PORT, host: HOST })

    next = sinon.spy()

    sinon.stub(server, 'expose').callsFake(() => {})
    sinon.spy(server, 'decorate')

    plugin.register(server, {
      models: ['my/models/**'],
      name: 'sqlite',
      connection: 'sqlite://db.sqlite/api',
      settings: {
        storage: ':memory:'
      }
    }, next)

    expect(server.expose).to.be.calledOnce
    expect(server.decorate).to.be.callCount(6)
  })

  afterEach(() => {
    server.expose.restore()
    server.decorate.restore()
  })

  it('should expose data source under name specified in options', () => {
    expect(server.expose)
      .to.be.calledOnce
      .and.calledWithExactly('sqlite', sinon.match.any)
  })

  it('should provide method to fetch specific model', () => {
    expect(server.decorate).calledWith('server', 'getModel', sinon.match.func)
  })

  it('should provide method to register models', () => {
    expect(server.decorate).calledWith('server', 'registerModels', sinon.match.func)
  })

  it('should provide method to register single model', () => {
    expect(server.decorate).calledWith('server', 'registerModel', sinon.match.func)
  })

  it('should expose remote methods during model registration', () => {
    const remoteMethods = sinon.spy()
    const modelFactory = (db, DataType, server) => {
      const Item = db.define('item', {
        name: DataType.STRING
      })

      Item.remoteMethods = remoteMethods

      return Item
    }

    return server.registerModel(modelFactory).then(() => {
      expect(remoteMethods).to.have.been.calledOnce
    })
  })

  it('should throw on invalid options', () => {
    plugin.register(server, {}, next)

    expect(next).to.be.calledWith(sinon.match.has('name', 'ValidationError'))
  })
})
