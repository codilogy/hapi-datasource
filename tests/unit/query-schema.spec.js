/* eslint-env node, mocha */

const expect = require('chai').expect
const QuerySchema = require('../../plugin/schema-mapper/joi/schemas/query')
const Joi = require('joi')
const {attributes} = require('../fixtures/model')
const $ = value => `&#x${value.charCodeAt(0).toString(16)};${value.substr(1)}`

describe('QuerySchema', () => {
  let qs
  const joinOperators = ['$and', '$or']
  const GENERIC_COMPARISION_OPERATORS = [
    '$eq', '$ne'
  ]
  const NUMBER_COMPARISION_OPERATORS = [
    '$lt', '$lte', '$gt', '$gte'
  ]
  const STRING_COMPARISION_OPERATORS = [
    '$like', '$notLike', '$iLike', '$notILike'
  ]
  const NUMERIC_RANGE_OPERATORS = [
    '$between', '$notBetween'
  ]
  const STRICT_TYPE_RANGE_OPERATORS = [
    '$in', '$notIn'
  ]
  const LOOSE_TYPE_RANGE_OPERATORS = [
    '$any'
  ]

  beforeEach(() => {
    qs = QuerySchema({attributes})
  })

  describe('general rules', () => {
    it('should allow where queries', () => {
      expect(Joi.validate({where: {}}, qs))
        .to.have.property('error', null)
    })
    it('should forbid unknown properties', () => {
      expect(Joi.validate({select: {}}, qs))
        .to.have.property('error').and.is.instanceof(Error)
    })
  })

  joinOperators.map(operator => {
    describe(`${operator} operator`, () => {
      it(`should be allowed in where clause as an array of objects`, () => {
        expect(Joi.validate({where: {[operator]: [{}]}}, qs))
          .to.have.property('error', null)
      })

      it(`should be allowed in where clause as an object of fields and operators`, () => {
        expect(Joi.validate({where: {[operator]: {[operator]: {}}}}, qs))
          .to.have.property('error', null)
      })
    })
  })

  describe('model attributes in where clause', () => {
    it(`should be allowed as an object`, () => {
      expect(Joi.validate({where: {createdAt: {}}}, qs))
        .to.have.property('error', null)
    })
    it(`should be allowed as scalar value in type inherited from model`, () => {
      expect(Joi.validate({where: {id: 1, name: 'name'}}, qs))
        .to.have.property('error', null)
    })
    it(`should fail if field has invalid type different then in model`, () => {
      expect(Joi.validate({where: {id: 'string'}}, qs))
        .to.have.nested.property('error.message')
        .that.contains('"id" must be a number')
        .that.contains('"id" must be an object')
    })
    it(`should fail when unknown attribute is used`, () => {
      expect(Joi.validate({where: {unknownField: 1}}, qs))
        .to.have.nested.property('error.message').that.contains('"unknownField" is not allowed')
    })
    it('should fail if attribute is deeply nested', () => {
      expect(Joi.validate({where: {$and: [{id: {$or: [{id: 1}]}}]}}, qs))
        .to.have.nested.property('error.message')
        .that.contains('"id" is not allowed')
    })
  })

  describe('generic comparision operators', () => {
    GENERIC_COMPARISION_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute in type inherited from model`, () => {
        expect(Joi.validate({where: {id: {[operator]: 1}}}, qs))
          .to.have.property('error', null)
      })
      it(`${operator} should fail if operator type is different then model type`, () => {
        expect(Joi.validate({where: {id: {[operator]: 'string'}}}, qs))
          .to.have.nested.property('error.message')
          .that.contains(`"${$(operator)}" must be a number`)
      })
    })
  })

  describe('number comparision operators', () => {
    NUMBER_COMPARISION_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute`, () => {
        expect(Joi.validate({where: {id: {[operator]: 1}}}, qs))
          .to.have.property('error', null)
      })
      it(`${operator} should accept numeric values`, () => {
        [1.2].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.property('error', null)
        })
      })
      it(`${operator} should fail with types other then number`, () => {
        ['string', [], {}].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.nested.property('error.message')
            .that.contains(`"${$(operator)}" must be a number`)
        })
      })
    })
  })

  describe('string comparision operators', () => {
    STRING_COMPARISION_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute`, () => {
        expect(Joi.validate({where: {id: {[operator]: 'string'}}}, qs))
          .to.have.property('error', null)
      })
      it(`${operator} should accept string and object values`, () => {
        ['string', {}].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.property('error', null)
        })
      })
      it(`${operator} should fail with types other then number`, () => {
        [1, []].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.nested.property('error.message')
            .that.contains(`"${$(operator)}" must be a string`)
        })
      })
    })
  })

  describe('numeric range operators', () => {
    NUMERIC_RANGE_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute`, () => {
        expect(Joi.validate({where: {id: {[operator]: [1, 2]}}}, qs))
          .to.have.property('error', null)
      })
      it(`${operator} should fail with type different then array`, () => {
        ['string', 1, {}].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.nested.property('error.message')
            .that.contains(`"${$(operator)}" must be an array`)
        })
      })
      it(`${operator} should fail when array have invalid length`, () => {
        [[], [1], [1, 2, 3]].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.nested.property('error.message')
            .that.contains(`"${$(operator)}" must contain 2 items`)
        })
      })
      it(`${operator} should fail when array values have invalid type`, () => {
        [['x', 'y']].map(value => {
          expect(Joi.validate({where: {id: {[operator]: value}}}, qs))
            .to.have.nested.property('error.message')
            .that.contains(`"0" must be a number`)
        })
      })
    })
  })

  describe('strict type range operators', () => {
    STRICT_TYPE_RANGE_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute`, () => {
        expect(Joi.validate({where: {id: {[operator]: [1, 2]}}}, qs))
          .to.have.property('error', null)
      })
    })
  })

  describe('loose type range operators', () => {
    LOOSE_TYPE_RANGE_OPERATORS.map(operator => {
      it(`${operator} should be allowed as a property of an attribute`, () => {
        expect(Joi.validate({where: {id: {[operator]: [1, 2]}}}, qs))
          .to.have.property('error', null)
      })
    })
  })
})
