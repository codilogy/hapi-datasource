/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon')
const applyScopeToModel = require('../../plugin/handlers/scope.helper')

chai.use(require('sinon-chai'))

describe('scope helper', () => {
  let model

  beforeEach(() => {
    model = {
      unscoped: sinon.spy(() => model),
      scope: sinon.spy(() => model)
    }
  })

  it('should translate string to array', () => {
    applyScopeToModel(model, 'max')
    expect(model.scope).to.have.been.calledWithExactly(['max'])
  })

  it('should pass array forward', () => {
    applyScopeToModel(model, ['max', 'min'])
    expect(model.scope).to.have.been.calledWithExactly(['max', 'min'])
  })

  it('should translate object to Sequelize {method:string[]} format', () => {
    applyScopeToModel(model, {search: 'string'})
    expect(model.scope).to.have.been.calledWithExactly([{ method: ['search', 'string'] }])
  })

  it('should translate object with multiple arguments to Sequelize {method:string[]} format', () => {
    applyScopeToModel(model, {search: ['a', 'b']})
    expect(model.scope).to.have.been.calledWithExactly([{ method: ['search', 'a', 'b'] }])
  })

  it('should support array of multiple scopes', () => {
    applyScopeToModel(model, ['min', {search: ['a', 'b']}])
    expect(model.scope).to.have.been.calledWithExactly(['min', { method: ['search', 'a', 'b'] }])
  })

  it('should return unscoped model when scope is set to false', () => {
    expect(applyScopeToModel(model, false)).to.eql(model)
    expect(model.unscoped).to.have.been.calledOnce
  })

  it('should return untouched model when scope undefined', () => {
    expect(applyScopeToModel(model)).to.eql(model)
    expect(model.unscoped).to.have.callCount(0)
    expect(model.scope).to.have.callCount(0)
  })

  it('should return untouched model when scope null', () => {
    expect(applyScopeToModel(model, null)).to.eql(model)
    expect(model.unscoped).to.have.callCount(0)
    expect(model.scope).to.have.callCount(0)
  })
})
