/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon')
const SchemaWrapper = require('../../plugin/schema-mapper/joi/wrapper')

chai.use(require('sinon-chai'))

describe('JoiWrapper', () => {
  it('should warn when trying to use unknown schema property', () => {
    const wrapper = new SchemaWrapper('number')

    sinon.stub(console, 'error')
    wrapper.set('email')

    expect(console.error).to.have.been.calledWith('"Joi.number" has no method "email"')
    console.error.restore()
  })
})
